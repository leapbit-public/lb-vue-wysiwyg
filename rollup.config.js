import vue from 'rollup-plugin-vue'; // Handle .vue SFC files
import buble from 'rollup-plugin-buble'; // Transpile/polyfill with reasonable browser support
import commonjs from 'rollup-plugin-commonjs';
import postcss from 'rollup-plugin-postcss'

export default {
    input: 'wrapper.js', // Path relative to package.json
    output: {
        name: 'LbVueWysiwyg',
        exports: 'named',
    },
    external: ['debounce', 'prismjs', 'vue-prism-editor', 'vue2-dropzone'],
    plugins: [
        commonjs(),
        postcss(),
        vue({
            css: true, // Dynamically inject css as a <style> tag
            compileTemplate: true, // Explicitly convert template to render function
        }),
        buble({
            transforms: { forOf: false },
            objectAssign: 'Object.assign'
        }) // Transpile to ES5
    ]
};