
<br><br>

<h3>lb-vue-wysiwyg</h3>

<br>
<p><img src="https://www.leapbit.com/branding/logo.png" style="width: 300px;"></p>

## Demo

<a href="https://codesandbox.io/s/lb-vue-wysiwyg-9nr3p">Demo</a>

<br>

## Install

``` bash
npm install lb-vue-wysiwyg --save
```

Include plugin in your `main.js` file.

```js
import Vue from 'vue'
import wysiwyg from "lb-vue-wysiwyg";
Vue.use(wysiwyg, {
  hideModules: {
    bold: false
  }
});
```

In your components:
```html
<wysiwyg v-model="html" />
```

## Config options

All keys are optional.

```js
{
  // { [module]: boolean (set true to hide) }
  hideModules: { "bold": true },

  // you can override icons too, if desired
  // just keep in mind that you may need custom styles in your application to get everything to align
  iconOverrides: { "bold": "<i class='your-custom-icon'></i>" },

  // if the image option is not set, images are inserted as base64
  image: {
    uploadURL: "/api/myEndpoint",
    dropzoneOptions: {}
  },

  // limit content height if you wish. If not set, editor size will grow with content.
  maxHeight: "500px",

  // set to 'true' this will insert plain text without styling when you paste something into the editor.
  forcePlainTextOnPaste: true
}
```
Available Modules:
 - bold
 - italic
 - underline
 - fontsize
 - fontcolor
 - backgroundcolor
 - justifyLeft
 - justifyCenter
 - justifyRight
 - headings
 - link
 - code
 - orderedList
 - unorderedList
 - image
 - table
 - removeFormat
 - separator

Note on the image upload API endpoint:
- Image is uploaded by `multipart/form-data`
- Your endpoint must respond back with a string, the URL for the image - e.g. `http://myapp.com/images/12345.jpg`
