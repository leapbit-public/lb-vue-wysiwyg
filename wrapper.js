// Import vue component
import component from './src/editor/index.js';

// To allow use as module (npm/webpack/etc.) export component
export default component;