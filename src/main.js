import Vue from 'vue';
import App from './App';
import "prismjs";
import "prismjs/themes/prism.css";

import wysiwyg from "./editor";
//import wysiwyg from "../dist/lb-vue-wysiwyg.js";
Vue.use(wysiwyg,
{
    maxHeight: "500px",
    forcePlainTextOnPaste: false,
    fileUpload(editor)
    {
        if(editor.selHtml == '')
        {
            editor.exec('insertHTML', `<a href="http://www.google.com">http://www.google.com</a>`);
        }
        else
        {
            editor.exec('insertHTML', `<a href="http://www.google.com">`+editor.selHtml+`</a>`);
        }
        
    }
});

new Vue(
{
    render: h => h(App)
}).$mount('#app')