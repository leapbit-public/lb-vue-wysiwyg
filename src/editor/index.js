import Editr from "./Editr.vue";

export default {
	install: (Vue, options = {}) => {
        // Apply configuration
        Editr.props.options = {
            type: Object,
            default: function () { return options }
        };
		Vue.component("wysiwyg", Editr);
	},

	component: Editr
}
