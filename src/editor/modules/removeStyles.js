module.exports = {
	title: "removeStyles",
	action: ["removeStyles"],
	description: "Remove styles",
	icon: '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve"><g><g transform="matrix(1 0 0 -1 0 1008)"><path d="M990,385.5L832.5,228L990,70.5L937.5,18L780,175.5L622.5,18L570,70.5L727.5,228L570,385.5l52.5,52.5L780,280.5L937.5,438L990,385.5z M875.3,998L850,928H572.9L343.5,298h-74.9l229.4,630H220l25.3,70H875.3z M10,158l25.3,70h525L535,158H10z"/></g></g></svg>'
}
